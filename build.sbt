import sbt.Keys._

name := "spark-paris-data-geek"

version := "1.0"

scalaVersion := "2.10.5"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.5.0" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.5.0" % "provided"

libraryDependencies += "com.typesafe" % "config" % "1.2.1" % "provided"

libraryDependencies+= "com.databricks" % "spark-csv_2.10" % "1.2.0"