package fr.creativedata.parisdatageek

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by aurelien on 11/09/15.
 */
object ParisDataGeek {

  def main (args: Array[String]) {

    var mode = "simple"

    if (!args.isEmpty){
      mode = args(0);
    }

    val sc = new SparkContext(new SparkConf().setAppName("SparkvsImpala")
      .set("spark.executor.memory", "7g")
      .set("spark.mesos.mesosExecutor.cores","22")
      .set("spark.sql.shuffle.partitions","22")
      .set("spark.broadcast.blockSize","16m")
      .set("spark.default.parallelism","33")
      .set("spark.sql.autoBroadcastJoinThreshold","20000000")
      .set("spark.speculation","true")
      .set("spark.mesos.coarse", "true"))

    val sqlContext = new SQLContext(sc)


    mode match {

      case "simple" => {
        initDataFrameForSimpleRequest(sqlContext)
        executeSimpleRequest(sqlContext)
      }
      case "complex" => {
        initDataFrameForComplexRequest(sqlContext)
        executeComplexRequest(sqlContext)
      }
      case "series" => {
        initDataFrameForSimpleRequest(sqlContext)
        initDataFrameForComplexRequest(sqlContext)
        for (x <- 1 to 5) {
          executeSimpleRequest(sqlContext)
        }

        executeComplexRequest(sqlContext)
        executeComplexRequest(sqlContext)

        for (x <- 1 to 5) {
          executeSimpleRequest(sqlContext)
        }
      }
    }

  }

  def initDataFrameForSimpleRequest(sqlContext: SQLContext): Unit = {

    val df = sqlContext.read.format("com.databricks.spark.csv").option("header", "false").option("delimiter", ";")
      .load("hdfs://37.187.79.74:8020/user/hive/warehouse/douzego/out.csv")

    df.cache()

    df.registerTempTable("simple")
  }

  def initDataFrameForComplexRequest(sqlContext: SQLContext): Unit = {

    val parquetFile1 = sqlContext.read.parquet("hdfs://37.187.79.74:8020/user/hive/warehouse/resultsparquet/").repartition(20)
    parquetFile1.cache()
    val parquetFile2 = sqlContext.read.parquet("hdfs://37.187.79.74:8020/user/hive/warehouse/resultsparquet2/").repartition(20)
    parquetFile2.cache()
    val parquetFile3 = sqlContext.read.parquet("hdfs://37.187.79.74:8020/user/hive/warehouse/resultsparquet3/").repartition(20)
    parquetFile3.cache()

    parquetFile1.registerTempTable("t1")
    parquetFile2.registerTempTable("t2")
    parquetFile3.registerTempTable("t3")
  }

  def executeSimpleRequest(sqlContext: SQLContext): Unit = {

    println("Requete simple : ")

    val simpleRequest = sqlContext.sql("select C0, count(*) from simple group by C0 order by 1")

    simpleRequest.map(r => r(0) + ", " + r(1)).collect().foreach(println)
  }

  def executeComplexRequest(sqlContext: SQLContext): Unit = {

    println("Requete complexe : ")

    val complexRequest = sqlContext.sql("select count(distinct t1.q1), max(t2.r1) from " +
      "(select t1.quantity as q1 from t1 left outer join t2 on t1.quantity = t2.quantity where t2.quantity between 314 and 500) as t1" +
      " left outer join (select quantity as q1, avg(quantity) as r1 from t3 group by quantity) as t2 on t1.q1=t2.q1")

    complexRequest.map(r => r(0) + ", " + r(1)).collect().foreach(println)
  }




}
